#!/bin/bash

swapoff -a

tmp=`mktemp`
grep -v swap /etc/fstab > $tmp
cat $tmp >/etc/fstab
rm $tmp

echo 'lukapo   ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/lukapo

export DEBIAN_FRONTEND=noninteractive

echo ${node_name} > /etc/hostname
echo 127.0.0.1 ${node_name} >> /etc/hosts
hostname ${node_name}

export DEBIAN_FRONTEND=noninteractive

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

echo 'nameserver 8.8.8.8' > /etc/resolv.conf

sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1

curl -s https://s3-eu-west-1.amazonaws.com/deploy-server/latest/deploy_standard_settings.sh > /opt/deploy_standard_settings.sh
bash /opt/deploy_standard_settings.sh 2>/opt/deploy_standard_settings.error.log >/opt/deploy_standard_settings.log

usermod lukapo -s /bin/bash

test -e /usr/bin/python || (apt -y update && apt install -y python python-pip)

apt-get update
apt-get -y install apt-transport-https ca-certificates

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update && apt-get install -y apt-transport-https curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

apt-get update
apt-get install -y docker-ce=18.06.1~ce~3-0~ubuntu kubelet=1.13.5-00 kubeadm=1.13.5-00 kubectl=1.13.5-00
apt-mark hold docker-ce kubelet kubeadm kubectl

echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
sysctl -p

kubeadm init --pod-network-cidr=10.244.0.0/16

mkdir -p /root/.kube

cp -i /etc/kubernetes/admin.conf /root/.kube/config
chown $(id -u):$(id -g) /root/.kube/config

sleep 60

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml