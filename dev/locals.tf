locals {
  environment = "dev"
  project = "sdac"
  vpc_cidr        = "10.99.0.0/16"
  vpc_cidr_prefix = "10.99"
  aws_region = "eu-west-3"
}