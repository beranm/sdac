module "vpc" {
  source = "github.com/terraform-aws-modules/terraform-aws-vpc.git?ref=v2.0.0"
  name   = var.vpc_name
  cidr   = var.cidr

  azs                          = var.azs
  private_subnets              = var.private_subnets
  public_subnets               = var.public_subnets
  database_subnets             = var.database_subnets
  create_database_subnet_group = var.create_database_subnet_group
  enable_dns_support           = var.enable_dns_support
  enable_dns_hostnames         = var.enable_dns_hostnames
  enable_nat_gateway           = var.enable_nat_gateway
  single_nat_gateway           = var.single_nat_gateway
  tags                         = var.tags

  enable_dhcp_options = false
}

