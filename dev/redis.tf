module "elasticache" {
  source = "github.com/beranm14/tf_aws_elasticache_redis"

  env            = local.environment
  name           = "redis"
  redis_clusters = 1
  redis_failover = false
  subnets        = module.vpc.private_subnets
  vpc_id         = module.vpc.vpc_id
  allowed_cidr   = [local.vpc_cidr]
  redis_version = "5.0.5"
  redis_node_type = "cache.t2.micro"

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

#resource "aws_security_group_rule" "allow_all" {
#  type            = "ingress"
#  from_port       = 6379
#  to_port         = 6379
#  protocol        = "tcp"
#  cidr_blocks = ["0.0.0.0/0"]
#
#  security_group_id = module.elasticache.redis_security_group_id
#}
# 
#
#resource "aws_security_group_rule" "allow_out_all" {
#  type            = "egress"
#  from_port       = 0
#  to_port         = 65535
#  protocol        = "tcp"
#  cidr_blocks = ["0.0.0.0/0"]
#  security_group_id = module.elasticache.redis_security_group_id
#}
#
#


output "mw-redis-endpoint" {
  value = module.elasticache.endpoint
}
