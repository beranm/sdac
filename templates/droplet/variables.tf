
variable "instance_setting" {
  description = "Instance settings"
  type = object({
    image     = string
    region    = string
    user_data = string
    size      = string
    name      = string
  })
}
