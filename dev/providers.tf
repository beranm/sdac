provider "linode" {}

provider "cloudflare" {}

provider "digitalocean" {}

provider "aws" {
  region = "eu-west-3"
}

terraform {
  backend "s3" {
    bucket         = "sdac-terraform-tfstate-009264348660"
    key            = "dev"
    region         = "eu-west-3"
    dynamodb_table = "sdac-terraform-locks"
  }
}