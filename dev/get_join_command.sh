#!/bin/bash
set -e
OUTPUT=`ssh -o "StrictHostKeyChecking no" lukapo@${1} sudo kubeadm token create --print-join-command --ttl 0`
jq -n --arg output "$OUTPUT" '{"output":$output}' | cat
