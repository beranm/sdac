
module "fargate" {
  source = "git@github.com:lukaspour/tf_aws_ecs_fargate.git"

  name_prefix = "fargate-test-cluster"
  vpc_id      = module.vpc.vpc_id
  subnet_ids  = module.vpc.public_subnets

  internal_elb = false

  containers_definitions = {
    api = {
      task_container_image            = "beranm14/sdac-server:latest"
      task_container_assign_public_ip = true
      task_container_port             = 8090
      task_container_environment = [
        {
          name = "REDIS_SERVER"
          value = module.elasticache.endpoint
        }
      ]
      rule_field = "host-header"
      rule_values = ["api.statusoid.pl"]
      health_check = {
        port = "traffic-port"
        path = "/"
      }
      scaling_enable = true
    }
  }

  tags = {
    environment = "dev"
  }
}

output "load_balancer_domain" {
  description = "Get DNS record of load balancer"
  value       = module.fargate.load_balancer_domain
}


resource "cloudflare_record" "api" {
  zone_id = cloudflare_zone.statusoid.id
  type   = "CNAME"
  name   = "api"
  value  = module.fargate.load_balancer_domain
  ttl    = 120
}
