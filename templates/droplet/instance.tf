

resource "digitalocean_droplet" "droplet" {
  image     = var.instance_setting.image
  name      = var.instance_setting.name
  region    = var.instance_setting.region
  size      = var.instance_setting.size
  user_data = var.instance_setting.user_data
}
