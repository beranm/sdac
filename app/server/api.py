import json
import logging
from flask import Flask, request, render_template
import redis
import os

logging.basicConfig(level=logging.DEBUG)

redis_host = os.environ.get('REDIS_SERVER', 'localhost')
auth_token = os.environ.get('AUTH_TOKEN', '123456789')

app = Flask(__name__)

urls = [
    'https://www.seznam.cz/',
    'https://techmeetup.cz/',
    'https://www.lukapo.cz/',
    'https://www.statusoid.com/',
]


@app.route('/api', methods=['POST'])
def api_post():
    r = redis.StrictRedis(host=redis_host, decode_responses=True)
    data = request.json
    print(data)
    if 'x-auth-token' in request.headers and request.headers['x-auth-token'] == auth_token and 'url' in data and 'latency' in data:
        try:
            logging.info(data['url'] + " " + data['location'] + " " + str(data['latency']))
            r.zadd(data['url'], {data['location']: data['latency']})
        except redis.exceptions.ResponseError:
            logging.info("Failed, cleared key")
            r.delete(data['url'])
    else:
        return json.dumps({'status': "bugger off"})
    return json.dumps({'status': "ok"})


@app.route('/url', methods=['GET'])
def url_get():
    return json.dumps(urls)


@app.route('/', methods=['GET'])
def root_get():
    r = redis.StrictRedis(host=redis_host, decode_responses=True)
    try:
        url_measurements = {}
        for url in urls:
            url_measurements[url] = r.zrange(url, 0, -1, withscores=True)
        return render_template(
                "index.html",
                url_measurements=url_measurements,
            )
    except redis.exceptions.ResponseError:
        return "Nothing measured yet!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8090)

