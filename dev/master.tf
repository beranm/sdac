
data "template_file" "master" {
  template = file("./tpl/install_master.sh.tpl")
  vars = {
    node_name = "master"
  }
}

module "master" {
  source = "../templates/droplet"

  instance_setting = {
    image     = "ubuntu-18-04-x64"
    name      = "master"
    region    = "fra1"
    size      = "s-2vcpu-2gb"
    user_data = data.template_file.master.rendered
  }
}

output "master_ip" {
  value = module.master.ipv4_address
}

data "external" "get_connection_string" {
  program = ["bash", "get_join_command.sh", module.master.ipv4_address]
}

output "kubeadm_join" {
  value = data.external.get_connection_string.result["output"]
}