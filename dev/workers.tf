
data "template_file" "worker1" {
  template = file("./tpl/install_node.sh.tpl")
  vars = {
    node_name = "worker1"
    join_command = data.external.get_connection_string.result["output"]
  }
}

module "worker1" {
  source = "../templates/linode"

  instance_setting = {
    image     = "ubuntu-18-04-x64"
    name      = "worker1"
    region    = "ap-west"
    size      = "g6-nanode-1"
    user_data = data.template_file.worker1.rendered
  }
}

output "worker1_ip" {
  value = module.worker1.ipv4_address
}


data "template_file" "worker2" {
  template = file("./tpl/install_node.sh.tpl")
  vars = {
    node_name = "worker2"
    join_command = data.external.get_connection_string.result["output"]
  }
}

module "worker2" {
  source = "../templates/linode"

  instance_setting = {
    image     = "ubuntu-18-04-x64"
    name      = "worker2"
    region    = "us-central"
    size      = "g6-nanode-1"
    user_data = data.template_file.worker2.rendered
  }
}

output "worker2_ip" {
  value = module.worker2.ipv4_address
}


data "template_file" "worker3" {
  template = file("./tpl/install_node.sh.tpl")
  vars = {
    node_name = "worker3"
    join_command = data.external.get_connection_string.result["output"]
  }
}


module "worker3" {
  source = "../templates/linode"

  instance_setting = {
    image     = "ubuntu-18-04-x64"
    name      = "worker3"
    region    = "ap-northeast"
    size      = "g6-nanode-1"
    user_data = data.template_file.worker3.rendered
  }
}

output "worker3_ip" {
  value = module.worker3.ipv4_address
}