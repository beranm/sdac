# Statusoid as a code

Repo for presentation at tech meetup at Ostrava 2019

# HOWTO

Since I forget a lot, I need this to copy & paste day or two before the presentation.

```bash
terraform init && terraform plan

# BACKEND INIT
terraform apply -auto-approve -target=module.elasticache -target=module.vpc
terraform apply -auto-approve -target=module.fargate
terraform apply -auto-approve -target=cloudflare_zone.statusoid -target=cloudflare_record.api

# CREATE K8S MASTER
terraform apply -auto-approve -target=module.master

sleep 120; # Master is a bit slow guy

# CREATE MINIONS
terraform apply -auto-approve -target=module.worker1
```

