resource "random_string" "password" {
  length  = 16
  special = false
}

locals {
  lookup_image = {
    ubuntu-18-04-x64 : "linode/ubuntu18.04"
  }
}

resource "linode_instance" "worker" {
  label           = var.instance_setting.name
  image           = local.lookup_image[var.instance_setting.image]
  region          = var.instance_setting.region
  type            = var.instance_setting.size
  authorized_keys = []
  root_pass       = random_string.password.result
  stackscript_id  = linode_stackscript.worker.id
}

resource "linode_stackscript" "worker" {
  label       = "Deploy custom user_data"
  description = "Deploy custom user_data"
  script      = var.instance_setting.user_data
  images      = ["linode/ubuntu18.04", "linode/ubuntu16.04lts"]
  rev_note    = "Deploy custom user_data"
}
