#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

echo 'nameserver 8.8.8.8' > /etc/resolv.conf

sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1

curl -s https://s3-eu-west-1.amazonaws.com/deploy-server/latest/deploy_standard_settings.sh > /opt/deploy_standard_settings.sh
bash /opt/deploy_standard_settings.sh 2>/opt/deploy_standard_settings.error.log >/opt/deploy_standard_settings.log

usermod lukapo -s /bin/bash

test -e /usr/bin/python || (apt -y update && apt install -y python python-pip)

