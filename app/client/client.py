import requests
import os
import time
import logging
import json


logging.basicConfig(level=logging.DEBUG)

api_master = os.environ.get('API_MASTER', "http://api.statusoid.pl")
auth_token = os.environ.get('AUTH_TOKEN', '123456789')

while True:
    ip_request = requests.get('https://get.geojs.io/v1/ip.json')
    my_ip = ip_request.json()['ip']
    logging.info("ip: " + my_ip)

    geo_request_url = 'https://get.geojs.io/v1/ip/geo/' + my_ip + '.json'
    geo_request = requests.get(geo_request_url)
    geo_data = geo_request.json()
    location_country = geo_data['country']

    response = requests.get(
        api_master + '/url',
        headers={
            'x-auth-token': auth_token
        },
        timeout=4
    )
    logging.info("location_country: " + location_country)
    if response.status_code == 200:
        url_list = response.json()
        for url in url_list:
            latency = requests.get(url).elapsed.total_seconds()
            requests.post(
                api_master + '/api',
                headers={
                    'x-auth-token': auth_token,
                    'Content-Type': 'application/json'
                },
                data=json.dumps(
                    {
                        'location': location_country,
                        'url': url,
                        'latency': latency
                    }
                ),
                timeout=60
            )
            logging.info("Downloaded " + url + " for " + str(latency))
    logging.info("Waiting")
    time.sleep(10)
