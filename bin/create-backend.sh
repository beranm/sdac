AWS_REGION="eu-west-3"
ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"

aws s3api create-bucket \
	--region "${AWS_REGION}" \
	--bucket "sdac-terraform-tfstate-${ACCOUNT_ID}" \
    --create-bucket-configuration LocationConstraint="${AWS_REGION}"

aws s3api put-bucket-versioning --bucket "sdac-terraform-tfstate-${ACCOUNT_ID}" --versioning-configuration Status=Enabled

aws dynamodb create-table \
	--region "${AWS_REGION}" \
	--table-name "sdac-terraform-locks" \
	--attribute-definitions AttributeName=LockID,AttributeType=S \
	--key-schema AttributeName=LockID,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 
